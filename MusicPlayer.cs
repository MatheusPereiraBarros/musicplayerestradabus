﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine.UI;

 public class MusicPlayer : MonoBehaviour{
    //Enumerador que controla o valor do índice da lista de músicas
     public enum SeekDirection { Forward, Backward }
     public AudioSource source;
     //Cria estrutura de dados que armazena em tempo de execução os arquivos de áudio lidos na pasta
     [SerializeField] [HideInInspector] public List<AudioClip> clips = new List<AudioClip>();
     //Mostra o nome da música atualmente executada
     public Text nomeMusica;

     public Text tempoMusica;
     float timer; 
     string seconds; 
     string minutes;
     //Controla o índice da estrutura de dados que armazena os arquivos de áudio
     [SerializeField] [HideInInspector] private int currentIndex = 0;

     //Atributo que lida com os arquivos de áudio de maneira direta
     private FileInfo[] soundFiles;
     //Extensões reconhecidas pela Unity3D em modo Web/Standalone
     private List<string> validExtensions = new List<string> { 
         ".ogg", ".wav", ".mp3" 
         }; 
     //Caminho onde os arquivos de áudio serão extraídos
     public string absolutePath = "C://music"; // relative path to where the app is running - change this to "./music" in your case
      void Start(){
         //Invoca o método ReloadSounds() que varre os arquivos de extensão ogg. e wav. na pasta declarada no atributo absolutePath
         ReloadSounds();
         
     }
 
     //Oferece algumas opções como Pular Faixa e Reler arquivos da pasta chamando 
     void OnGUI(){
         if (GUILayout.Button("Previous")) {
             Seek(SeekDirection.Backward);
             PlayCurrent();
         }
         if (GUILayout.Button("Play current")) {
             PlayCurrent();
         }
         if (GUILayout.Button("Next")) {
             Seek(SeekDirection.Forward);
             PlayCurrent();
         }
         if (GUILayout.Button("Reload")) {
             ReloadSounds();
         }
         if (GUILayout.Button("Change Track")) {
             ChangeTrack();
         }
     }

     //Muda música automaticamente no final 
     void ChangeTrack(){
         Seek(SeekDirection.Forward);
         PlayCurrent();
     }
     
     void Seek(SeekDirection d)
     {
         if (d == SeekDirection.Forward)
             currentIndex = (currentIndex + 1) % clips.Count;
         else {
             currentIndex--;
             if (currentIndex < 0) currentIndex = clips.Count - 1;
         }
     }
 
     void PlayCurrent(){  
        //print(clips[currentIndex].isReadyToPlay);
        //source.clip = clips[currentIndex];
        // source.Play();

        //Verifica se os arquivos de áudio foram lidos com sucesso 
        if(clips[currentIndex].loadState == AudioDataLoadState.Loaded){
         source.clip = clips[currentIndex];
         source.Play();
         print(source.clip.name);
         nomeMusica.text = source.clip.name;
         //Muda música automaticamente no final 
         Invoke("ChangeTrack", source.clip.length);

         }
        
     }

     
     void Update(){
         if(source.isPlaying){
             
              timer = source.time;
              minutes = Mathf.Floor(timer / 60).ToString("00");
              seconds = Mathf.Floor(timer % 60).ToString("00");

              //print(minutes + ":" + seconds);

              tempoMusica.text = string.Concat(minutes + ":" + seconds);
         }
     }
 
     void ReloadSounds(){
         //Esvazia a estrutura de dados com os arquivos de áudio posteriormente lidos
         clips.Clear();
         //Obtém as meta-informações de todos os arquivos na pasta declarada
         var info = new DirectoryInfo(absolutePath);
         /*Lê todos os arquivos válidos encontrados na pasta através da variável info
         e adiciona apenas aqueles que possuem as extensões descritas no atributo validExtensions
         (no caso .wav e .ogg)*/
         soundFiles = info.GetFiles().Where(f => IsValidFileType(f.Name)).ToArray();
 
         
         //Laço que invoca a corotina LoadFile(), passando como parâmetro o nome completo dos arquivos lidos 
         foreach (var s in soundFiles)
             StartCoroutine(LoadFile(s.FullName));
     }
 
     //Método que verifica se os arquivos lidos possuem as extensões descritas no atributo validExtensions
     bool IsValidFileType(string fileName){
         return validExtensions.Contains(Path.GetExtension(fileName));
     }
 
     /*A seguinte corotina lê o nome dos arquivos passados como parâmetros 
     e os adiciona a cena na Unity3D em tempo de execução como arquivos de áudio usando a classe WWW;
     posteriormente os adicionando a estrutura de dados "clips".
      */
     IEnumerator LoadFile(string path){
        //Obtém arquivo por arquivo, armazenando seus nomes completos na variável "www"
        WWW www = new WWW("file:///" + path);
        print("loading " + path);
        /*Cria um novo arquivo de áudio através do arquivo obtido pela variável "www"
        Utiliza técnica de método extraído através de NewMethod()*/
        AudioClip clip;
       if(path.Contains(".mp3")){
            clip = NewMethod1(www);
        }
        else{
            clip = NewMethod(www);       
        }
        
        //Obtém o nome do arquivo de áudio
        clip.name = Path.GetFileName(path);
        //Adiciona o arquivo de áudio a estrutura de dados "clips";
        clips.Add(clip);
        

        print("done loading");
        //Controla o fluxo de execução retornando o valor de www a cada execução da corotina
        yield return www;

    }

    private static AudioClip NewMethod1(WWW www){
        return NAudioPlayer.FromMp3Data(www.bytes);
    }

    /*Recebe como parâmetro o atributo da classe WWW obtido na corotina LoadFile()
     retornando o mesmo como arquivo de áudio através do método GetAudioClipCompressed() da classe WWWAudioExtensions
    da Unity3D versão 5.6*/
    private static AudioClip NewMethod(WWW www){
        return www.GetAudioClipCompressed();
    }
}